﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRandomizer_testeo : MonoBehaviour
{
    public GameObject obstacleJump, obstacleDown, obstacleWall;
    public BoxCollider col;
    private int num;

   

    void Start()
    {
        col = GetComponent<BoxCollider>();
        
    }


    void Update()
    {
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "SetObstacle")
        {
            randomizeObstacle();
        }

        if (other.gameObject.tag == "ResetObstacle")
        {
            resetAll();
        }
        


    }

    public void OnTriggerExit(Collider other)
    {

    }

    public void OnCollisionEnter(Collision coll)
    {
       
    }

    public void resetAll()
    {
        col.enabled = true;
        obstacleJump.SetActive(false);
        obstacleDown.SetActive(false);
        obstacleWall.SetActive(false);
    }

    public void randomizeObstacle()
    {
        num = Random.Range(0, 4);

        switch (num)
        {
            case 0:
                obstacleJump.SetActive(true);
                break;
            case 1:
                obstacleJump.SetActive(true);
                break;
            case 2:
                obstacleDown.SetActive(true);
                break;
            case 3:
                obstacleWall.SetActive(true);
                break;
            default:
                break;
        }

        //col.enabled = false;
    }
}

