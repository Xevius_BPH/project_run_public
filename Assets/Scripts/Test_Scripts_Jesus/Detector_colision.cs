﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector_colision : MonoBehaviour
{
    public BoxCollider box;
    public GameObject gameOver;
    private LevelControler levelScr;
    public Material texture;
   
    
    public bool dst=false;
    // Start is called before the first frame update
    void Start()
    {
        
        box = GetComponent<BoxCollider>();
        levelScr = GameObject.Find("Level").GetComponent<LevelControler>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider coll)
    {
     

        if (coll.gameObject.layer == 8)
        {
            if (coll.gameObject.tag == "Wall")
            {
                if (dst)
                {
                    coll.gameObject.GetComponent<Animator>().Play("disolver");
                    
                }
                else
                {
                    Time.timeScale = 0;
                    levelScr.stopAll();
                    gameOver.SetActive(true);
                }
            }
            else
            {
                Time.timeScale = 0;
                levelScr.stopAll();
                gameOver.SetActive(true);
            }
            
        }
    }
    

    
}
