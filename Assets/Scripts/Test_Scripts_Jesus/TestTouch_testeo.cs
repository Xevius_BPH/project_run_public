﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestTouch_testeo : MonoBehaviour
{
    public Button jump,duck,dash;

    private PlayerControler levelScr;

    void Start()
    {
        jump.onClick.AddListener(jumpButton);
        duck.onClick.AddListener(duckButton);
        dash.onClick.AddListener(dashButton);
        levelScr = GameObject.Find("Player").GetComponent<PlayerControler>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void jumpButton()
    {
       
        levelScr.playerJump();
    }   
    public void duckButton()
    {
      
        levelScr.playerDuck();

    }   
    public void dashButton()
    {
       
        levelScr.playerDust();
    }
}
