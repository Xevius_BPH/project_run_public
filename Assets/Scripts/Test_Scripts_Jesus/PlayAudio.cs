﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{
    private AudioSource Music;

    void Start()
    {

        Music = GetComponent<AudioSource>();

    }
    public void Play()
    {
        Music.Play();
    }
    
}
