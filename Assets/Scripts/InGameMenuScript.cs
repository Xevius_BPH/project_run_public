﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameMenuScript : MonoBehaviour
{
    public Button restartGameBtn,resetBtn;

    void Start()
    {
        restartGameBtn.onClick.AddListener(restartGame);
        resetBtn.onClick.AddListener(resetLevel);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void restartGame()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void resetLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainScene");

    }
}
