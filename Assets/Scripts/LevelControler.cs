﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelControler : MonoBehaviour
{
    private Vector3 levMove = new Vector3(-10f, 0, 0);
    private Vector3 reset = new Vector3(0, 0, 0);

    public GameObject scorePanel,checkPanel;


    void Start()
    {
    }

    void Update()
    {
        transform.Translate(levMove*Time.deltaTime);



        if (transform.position.x < -320)
        {
            transform.position = reset;
            checkRewardStart();

        }
    }

    public void stopAll()
    {
        levMove = new Vector3(0, 0, 0);
    }

    public void checkRewardStart()
    {
        checkPanel.SetActive(true);
        scorePanel.GetComponent<ScoreScript>().scoreUPtest();
        Invoke("checkRewardEnd", 5);
    }

    public void checkRewardEnd()
    {
        checkPanel.SetActive(false);
    }


}
