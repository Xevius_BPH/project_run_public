﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public Rigidbody playerRigidBody;
   
    private bool grounded;
    public float speed;
    private Vector3 jump = new Vector3(0, 7, 0);
    public Animator anim;
    

    void Start()
    {
        playerRigidBody = GetComponent<Rigidbody>();
        
        
        grounded = false;
        
    }


    void Update()
    {
        
    }

    public void playerJump()
    {
        if (grounded)
        {
            anim.Play("jump");
        }
    }

    public void playerDuck()
    {
        if (grounded)
        {
            anim.Play("floor");
        }
    }

    public void playerDust()
    {
        if (grounded)
        {
            anim.Play("dush");
        }
    }

   

    public void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            grounded = true;
        }

        if (coll.gameObject.tag=="Obstacle")
        {
            Time.timeScale = 0;
        }

    }

    public void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            grounded = false;
        }
    }


}
