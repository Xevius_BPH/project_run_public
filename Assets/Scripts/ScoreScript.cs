﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text scoreLable;

    string scoreString;
    float scoreNum;
    int scoreInt;

    void Start()
    {
        //InvokeRepeating("scoreUP", 0, 0.5f);
    }

    void Update()
    {
        scoreUP();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            scoreUPtest();
        }
    }

   
    
    public void scoreUP()
    {
        scoreNum += Time.deltaTime*10;
        scoreInt = (int)scoreNum;
        scoreString = scoreInt.ToString("0000000000");
        scoreLable.text = scoreString;
    }

    public void scoreUPtest()
    {
        scoreNum +=1000;
        scoreInt = (int)scoreNum;
        scoreString = scoreInt.ToString("0000000000");
        scoreLable.text = scoreString;
    }
}

