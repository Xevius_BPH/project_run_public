﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRandomizer : MonoBehaviour
{
    public GameObject shape1, shape2, shape3,gameOver;
    public BoxCollider col;
    private int num;

    private LevelControler levelScr;

    void Start()
    {
        col = GetComponent<BoxCollider>();
        levelScr = GameObject.Find("Level").GetComponent<LevelControler>();
    }


    void Update()
    {
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "SetObstacle")
        {
            randomizeObstacle();
        }

        if (other.gameObject.tag == "ResetObstacle")
        {
            resetAll();
        }
        if (other.gameObject.tag == "PlayerHitBox")
        {
            Time.timeScale = 0;
            levelScr.stopAll();
            gameOver.SetActive(true);
        }


    }

    public void OnTriggerExit(Collider other)
    {

    }

    public void OnCollisionEnter(Collision coll)
    {
       
    }

    public void resetAll()
    {
        col.enabled = true;
        shape1.SetActive(false);
        shape2.SetActive(false);
        shape3.SetActive(false);
    }

    public void randomizeObstacle()
    {
        num = Random.Range(0, 4);

        switch (num)
        {
            case 0:
                shape1.SetActive(true);
                break;
            case 1:
                shape1.SetActive(true);
                break;
            case 2:
                shape2.SetActive(true);
                break;
            case 3:
                shape3.SetActive(true);
                break;
            default:
                break;
        }

        //col.enabled = false;
    }
}

