﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test_MA_DistanceCounter : MonoBehaviour
{
    public Text metersNum;

    float meters;
    int metersInt;
    string metersText;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        meters = meters + 0.01f;
        metersInt = (int)meters;
        metersText = metersInt.ToString("0000000000");

        metersNum.text = metersText;


    }
}
