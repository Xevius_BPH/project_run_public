﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_MA_Level : MonoBehaviour
{
    private Vector3 levMove = new Vector3(-0.02f,0,0);
    private Vector3 reset = new Vector3(0,0,0);

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(levMove);

        if (transform.position.x<-90)
        {
            transform.position = reset;
            
        }
    }
}
