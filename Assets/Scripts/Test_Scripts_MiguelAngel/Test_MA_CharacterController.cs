﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_MA_CharacterController : MonoBehaviour
{
    private Rigidbody rb;
    public GameObject playerModel;
    public float speed;
    public Vector3 movement = new Vector3(1, 0, 0);
    private Vector3 jump = new Vector3(0, 8, 0);
    private bool grounded;

    void Start()
    {
        //rb = playerModel.GetComponent<Rigidbody>();
        rb = GetComponentInChildren<Rigidbody>();
        speed = 3;
        grounded = false;
    }

    // Update is called once per frame
    void Update()
    {
      //  rb.velocity = movement * speed;

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(jump, ForceMode.Impulse);
        }
    }

    public void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag=="Ground")
        {
            grounded = true;
        }
    }

    public void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            grounded = false;
        }
    }


}
