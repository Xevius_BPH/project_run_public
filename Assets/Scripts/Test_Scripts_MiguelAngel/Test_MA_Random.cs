﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_MA_Random : MonoBehaviour
{
    public GameObject shape1, shape2, shape3;
    public BoxCollider col;
    private int num;

    void Start()
    {
        col = GetComponent<BoxCollider>();
    }

    
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Player")
        {
            randomizeObstacle();
        }

        if (other.gameObject.tag == "ResetObstacle")
        {
            resetAll();
        }

    }

    public void OnTriggerExit(Collider other)
    {
        
    }

    public void resetAll()
    {
        col.enabled = true;
        shape1.SetActive(false);
        shape2.SetActive(false);
        shape3.SetActive(false);
    }

    public void randomizeObstacle()
    {
        num = Random.Range(0, 4);

        switch (num)
        {
            case 0:
                break;
            case 1:
                shape1.SetActive(true);
                break;
            case 2:
                shape2.SetActive(true);
                break;
            case 3:
                shape3.SetActive(true);
                break;
            default:
                break;
        }

        //col.enabled = false;
    }
}
