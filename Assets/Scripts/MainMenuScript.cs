﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public Button startGameBtn,tutorialBtn,returnBtn;

    public GameObject mainPanel, tutorialPanel, tutorialbtnObject,gameTitle;
 

    void Start()
    {
        startGameBtn.onClick.AddListener(startGame);
        tutorialBtn.onClick.AddListener(openTutorial);
        returnBtn.onClick.AddListener(closeTutorial);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startGame()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void openTutorial()
    {
        mainPanel.SetActive(false);
        tutorialPanel.SetActive(true);
        tutorialbtnObject.SetActive(false);
        gameTitle.SetActive(false);
    }

    public void closeTutorial()
    {
        mainPanel.SetActive(true);
        tutorialPanel.SetActive(false);
        tutorialbtnObject.SetActive(true);
        gameTitle.SetActive(true);
    }
}
