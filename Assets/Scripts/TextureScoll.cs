﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureScoll : MonoBehaviour
{
    public float scrollX = -0.16f;
    public float scrollY = 0;

   
    void Update()
    {
        float OffsetX = Time.time * scrollX;
        float OffsetY = Time.time * scrollY;

        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(OffsetX, OffsetY);
    }
}
